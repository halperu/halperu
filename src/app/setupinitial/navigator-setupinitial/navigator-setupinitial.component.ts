import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/globals';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigator-setupinitial',
  templateUrl: './navigator-setupinitial.component.html',
  styleUrls: ['../../../assets/css/steps.css']
})
export class NavigatorSetupinitialComponent implements OnInit {
  datasteps:any = [];
  laststep:number=0
  urlstep:number=0
  urlflujo:number=0
  currentstep:number=0
  currentflujo:number=0
  
  lastflujo:number=0
  constructor(private router: Router,private globals: Globals) { 
    this.urlstep=parseInt(this.router.url.slice(-1));
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    this.currentstep=this.datasteps.step
    
    if(this.currentstep>this.laststep) this.laststep=this.currentstep;
    
    
    if(this.laststep<=3){
      this.currentflujo=1;
    }
    if(this.laststep==4){
      this.currentflujo=2;
    }
    if(this.laststep>=5 && this.laststep<=6){
      this.currentflujo=3;
    }
    if(this.laststep>=7){
      this.currentflujo=4;
    }
    

    if(this.urlstep<=3){
      this.urlflujo=1;
    }
    if(this.urlstep==4){
      this.urlflujo=2;
    }
    if(this.urlstep>=5 && this.urlstep<=6){
      this.urlflujo=3;
    }
    if(this.urlstep==7){
      this.urlflujo=4;
    }    
    if(this.currentflujo>this.lastflujo) this.lastflujo=this.currentflujo;
  }
  
  ngOnInit() {
  }

}
