import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { SetupinitialService } from '../services/setupinitial.service';
import { Globals } from '../globals';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step6',
  templateUrl: './step6.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step6Component {
  dataAdd: any={};
  datasteps: any={};
  new_gradoscursos:any=[];
  gradoscursos:any=[];
  grados: any=[];
  cursos: any=[];
  constructor(private gl_login: GlobalsLogin, private router: Router, private globals: Globals, private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    globals.ls_grados.forEach(element => {
      if(element.nivel==this.datasteps.nivel_upd.id){
        this.grados.push(element);
      }
    });

    this.setupinitialService.getCursos().subscribe((result) => {
      this.cursos=result
    }, (err) => {});    
    this.setupinitialService.getGradosCursos().subscribe((result) => {
      this.gradoscursos=result
    }, (err) => {});        
  }
  rowAdd(){
    let xgrado=this.dataAdd.grado.split('-');
    let xcurso=this.dataAdd.curso.split('-');
    let data={
      grado:xgrado[0],
      curso:xcurso[0]
    }
    let nd=this.setupinitialService.addGradosCursos(data).subscribe((result) => {
      this.gradoscursos.push(result);
    }, (err) => {});
    return nd;    
  }
  rowRemove(id,key){
    this.gradoscursos.splice(key, 1);
    let nd=this.setupinitialService.deleteGradoCurso(id).subscribe((result) => {

    }, (err) => {});    
  }


  onSubmit(){
    let stepcurrent=7;
    let nd=this.setupinitialService.centro_educativo_updsi({si_stepcurrent:stepcurrent}).subscribe((result) => {
      this.datasteps.step=stepcurrent;
      localStorage.ls_datasteps = JSON.stringify(this.datasteps);
      this.router.navigate(['setupinitial/step7'])
    }, (err) => {});   
  }
}
