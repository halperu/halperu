import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { LoginService } from '../services/login.service';
import { Globals } from '../globals';
import { GlobalsNologin } from '../globals-nologin';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class LoginComponent implements OnInit {
  datalogin:any = {};
  centros_educativos:any = [];
  colegio:any = 0;
  ls_datasteps:any={};
  constructor(private gl_nologin: GlobalsNologin,private router: Router, private globals: Globals, private route: ActivatedRoute, private loginService: LoginService ) { }
  
  ngOnInit() {
    let token = this.route.snapshot.paramMap.get("token")
    this.loginService.getCentrosEducativosbyToken(token).subscribe((data: {}) => {
      this.centros_educativos = data;
      this.colegio = data[0].colegio;
    }, error => this.router.navigate(['/']) );
  }

  onSubmit(){
    this.loginService.login(this.datalogin).subscribe((result) => {
      try{
        if(result.error){
          alert(result.error);
        }else{
          localStorage.clear();
          let urlredirect='';
          if(result.tipousuario == 2 && !result.si_end){
            // administrador de colegio
            this.ls_datasteps={
                "step":1,
                "nav":1,
                "niveles":[],
                "nivel_upd":{},
                "cursos":[],
                "leyendas":[],
                "data_nivel":{
                    "turnos":[],
                    "horas_academicas":1,
                    "ciclos":1,
                    "secciones":[],
                    "grados_cursos":[],
                }
            };
            localStorage.ls_datasteps = JSON.stringify(this.ls_datasteps)          
            const datasteps=JSON.parse(localStorage.ls_datasteps);
            if(result.si_niveles){
              let xsi_niveles=result.si_niveles.split("-")
              if(xsi_niveles){
                let xsi_datanivel=[]
                if(result.si_datanivel){
                  xsi_datanivel=JSON.parse(result.si_datanivel);
                }
                this.globals.ls_niveles.forEach(row_nivel => {
                  if(xsi_niveles.includes(String(row_nivel.id))){
                    datasteps.niveles.push(row_nivel)
                  }
                  if(result.si_nivelcurrent==row_nivel.id){
                    datasteps.nivel_upd=row_nivel
                  }
                  if(xsi_datanivel){
                    xsi_datanivel.forEach(row_dnivel => {
                      if(parseInt(row_dnivel.nivel) == parseInt(row_nivel.id)){
                        datasteps.data_nivel.turnos=row_dnivel.data.turnos_checked;
                        datasteps.data_nivel.horas_academicas=row_dnivel.data.horas_academicas;
                        datasteps.data_nivel.ciclos=row_dnivel.data.ciclos;
                      }
                    });
                  }
                });
              }
            }
            localStorage.ls_datasteps = JSON.stringify(datasteps);
            localStorage.setItem('userToken',result.token);
            localStorage.setItem('userCE',this.datalogin.centro_educativo);
            localStorage.setItem('userCOL',this.colegio);
            let si_stepcurrent=1;
            if(result.si_stepcurrent) si_stepcurrent=result.si_stepcurrent;
            localStorage.ls_stepcurrent = si_stepcurrent;
            urlredirect='/setupinitial/step'+si_stepcurrent;
            //this.router.navigate(['/setupinitial/step1']);
          }else{
            localStorage.removeItem('ls_datasteps');
            urlredirect='/dashboard'
          }
          localStorage.setItem('userToken',result.token);
          localStorage.setItem('userCE',this.datalogin.centro_educativo);
          localStorage.setItem('userCOL',this.colegio);
          this.router.navigate([urlredirect]);          
        }
      }catch(e){
        console.log(e);
        alert('Credenciales incorrecto');
      }
    }, (err) => {
      console.log(err);
    });
  }
}
