import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { Globals } from '../globals';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { SetupinitialService } from '../services/setupinitial.service';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step1Component {
  niveles:any = [];
  datasteps:any = [];
  form:any = {niveles:[]};
  
  constructor(private gl_login: GlobalsLogin, private router: Router, private globals: Globals, private formBuilder: FormBuilder, private setupinitialService: SetupinitialService){
    this.niveles=this.globals.ls_niveles;
    this.datasteps=JSON.parse(localStorage.ls_datasteps)
    this.form.niveles=this.niveles
    if(this.datasteps.niveles){
      this.form.niveles.forEach(nivel => {
        nivel.checked=false
        this.datasteps.niveles.forEach(nivel_checked => {
          if(nivel.id==nivel_checked.id){
            nivel.checked=true
          } 
        });
      });
    }
    
    
  }
  onSubmit(){
    let niveles_checked=[]
    let si_niveles=''
    this.form.niveles.forEach(element => {
      if(element.uchecked){
        if(si_niveles) si_niveles+='-'+element.id;
        else si_niveles+=element.id;
        niveles_checked.push({'id':element.id,'nombre':element.nombre})
        element.uchecked=false
      }
    });
    if(niveles_checked){
      let stepcurrent=2;
      let nd=this.setupinitialService.centro_educativo_updsi({si_niveles:si_niveles,si_stepcurrent:stepcurrent,}).subscribe((result) => {
        if(!result.error){
          this.datasteps.niveles=niveles_checked;
          this.datasteps.step=stepcurrent;
          localStorage.ls_datasteps = JSON.stringify(this.datasteps);
          this.router.navigate(['setupinitial/step2'])
        }
      }, (err) => {});
    }
  }
}