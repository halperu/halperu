import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { SetupinitialService } from '../services/setupinitial.service';
import { Globals } from '../globals';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step5Component {
  dataAdd: any={};
  datasteps: any={};
  new_cursos:any=[];
  cursos:any=[];
  constructor(private gl_login: GlobalsLogin, private router: Router, private globals: Globals, private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    this.setupinitialService.getCursos().subscribe((result) => {
      this.cursos=result
    }, (err) => {});
  }
  rowAdd(){
    let data={
      colegio:localStorage.userCOL,
      centro_educativo:localStorage.userCE,
      nombre:this.dataAdd.nombre
    }
    let nd=this.setupinitialService.addCursos(data).subscribe((result) => {
      this.cursos.push(result)
    }, (err) => {});
  }
  rowRemove(id,key){
    this.cursos.splice(key, 1);
    let nd=this.setupinitialService.deleteCurso(id).subscribe((result) => {

    }, (err) => {});
  }

  onSubmit(){
    let stepcurrent=6;
    let nd=this.setupinitialService.centro_educativo_updsi({si_stepcurrent:stepcurrent}).subscribe((result) => {
      this.datasteps.step=stepcurrent;
      localStorage.ls_datasteps = JSON.stringify(this.datasteps);
      this.router.navigate(['setupinitial/step6'])
    }, (err) => {});
  }
}
