import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpBackend } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private httpClient: HttpClient;
  httpOptions: any={};
  constructor(private globals: Globals, private http: HttpClient, handler: HttpBackend) { 
    localStorage.clear();
    this.httpClient = new HttpClient(handler);
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };    
  }
  ngOnInit() {
    
  }
  
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getCentrosEducativosbyToken(token_start): Observable<any> {
    return this.httpClient.get(this.globals.endpoint + 'centroeducativo/centros_educativos_por_colegio/?token=' + token_start,this.httpOptions ).pipe();
  }
  login(datalogin): Observable<any> {
    return this.httpClient.post<any>(this.globals.endpoint + 'login/', JSON.stringify(datalogin),this.httpOptions ).pipe(
      tap( (datalogin) => console.log('token') ),
      catchError(this.handleError<any>('login'))
    );
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
