import { Component } from '@angular/core';
import { GlobalsNologin } from '../globals-nologin';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  constructor(private gl_nologin: GlobalsNologin){

  }    
}