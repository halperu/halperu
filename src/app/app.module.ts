import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Routes,RouterModule} from "@angular/router";
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import {HomeComponent} from './home/home.component';
import {Step1Component} from './setupinitial/step1.component';
import {Step2Component} from './setupinitial/step2.component';
import {Step3Component} from './setupinitial/step3.component';
import {Step4Component} from './setupinitial/step4.component';
import {Step5Component} from './setupinitial/step5.component';
import {Step6Component} from './setupinitial/step6.component';
import {Step7Component} from './setupinitial/step7.component';
import {Step8Component} from './setupinitial/step8.component';
import { NavigatorSetupinitialComponent } from './setupinitial/navigator-setupinitial/navigator-setupinitial.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { NavigatorNologinComponent } from './navigator-nologin/navigator-nologin.component';
import { Globals, AuthorizationInterceptor } from './globals';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GlobalsLogin } from './globals-login';
import { GlobalsNologin } from './globals-nologin';

const appRoutes:Routes = [
  {path:'', component: HomeComponent},
  {path:'login/:token', component: LoginComponent},
  {path:'logout', component: LogoutComponent},
  
  {path:'setupinitial/step1', component: Step1Component},
  {path:'setupinitial/step2', component: Step2Component},
  {path:'setupinitial/step3', component: Step3Component},
  {path:'setupinitial/step4', component: Step4Component},
  {path:'setupinitial/step5', component: Step5Component},
  {path:'setupinitial/step6', component: Step6Component},
  {path:'setupinitial/step7', component: Step7Component},
  {path:'setupinitial/step8', component: Step8Component},
  {path:'dashboard', component: DashboardComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    Step5Component,
    Step6Component,
    Step7Component,
    Step8Component,
    DashboardComponent,
    NavigatorSetupinitialComponent,
    LoginComponent,
    NavigatorNologinComponent,
    DashboardComponent,
    LogoutComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
  ],
  providers: [
    Globals,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    },
    GlobalsLogin,
    GlobalsNologin
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }