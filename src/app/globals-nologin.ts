import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class GlobalsNologin {
    url_setupinitial: any=[
        '/setupinitial/step1',
        '/setupinitial/step2',
        '/setupinitial/step3',
        '/setupinitial/step4',
        '/setupinitial/step5',
        '/setupinitial/step6',
        '/setupinitial/step7',
        '/setupinitial/step8'
    ]
    constructor(private router: Router){
        if(localStorage.userToken){
            if(localStorage.ls_datasteps){
                if( !this.url_setupinitial.includes(this.router.url) ){
                    this.router.navigate(['/setupinitial/step'+localStorage.ls_stepcurrent]);
                }
            }else{
                this.router.navigate(['/dashboard']);
            }
        }
    }
}