import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { SetupinitialService } from '../services/setupinitial.service';
import { GlobalsLogin } from '../globals-login';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['../../assets/css/steps.css']
})
export class Step2Component {
  form: any={nivel:null};
  datasteps: any={};
  niveles: any={};
  si_nivelesupd: string='';
  constructor(private gl_login: GlobalsLogin, private router: Router,private setupinitialService: SetupinitialService){
    this.datasteps=JSON.parse(localStorage.ls_datasteps);
    this.setupinitialService.centro_educativo_getsi().subscribe((result) => {
      if(result.si_nivelesupd){
        let xsi_nivelesupd=result.si_nivelesupd.split('-')
        this.datasteps.niveles.forEach(element => {
          if(xsi_nivelesupd.includes(String(element.id))) element.settingok = true
          else element.settingok = false
        });
      }
    }, (err) => {});
  }
  onSubmit(){
    let xnivel=this.form.nivel.split('-')
    let stepcurrent=3;
    let nd=this.setupinitialService.centro_educativo_updsi({si_nivelcurrent:xnivel[0],si_stepcurrent:stepcurrent,}).subscribe((result) => {
      this.datasteps.nivel_upd={id:xnivel[0],nombre:xnivel[1]};
      this.datasteps.step=stepcurrent;
      localStorage.ls_datasteps = JSON.stringify(this.datasteps);
      this.router.navigate(['setupinitial/step3'])
    }, (err) => {});
  }
}
