import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { InitialService } from './services/initial.service';
import { Router } from '@angular/router';
//import { SetupinitialService } from './services/setupinitial.service';

@Injectable()
export class Globals {
    endpoint: string='http://100.25.67.64:8000/intranet/';
    ls_niveles: any=[{'id':1,'checked':false,'nombre':'inicial'},{'id':2,'checked':false,'nombre':'primaria'},{'id':3,'checked':false,'nombre':'secundaria'}];
    ls_turnos: any=[{'id':1,'checked':false,'nombre':'mañana'},{'id':2,'checked':false,'nombre':'tarde'},{'id':3,'checked':false,'nombre':'noche'}];
    ls_grados: any=[
        {
            "id": 1,
            "nombre": "1",
            "nivel": 2
        },
        {
            "id": 2,
            "nombre": "2",
            "nivel": 2
        },
        {
            "id": 3,
            "nombre": "3",
            "nivel": 2
        },
        {
            "id": 4,
            "nombre": "4",
            "nivel": 2
        },
        {
            "id": 5,
            "nombre": "5",
            "nivel": 2
        },
        {
            "id": 6,
            "nombre": "6",
            "nivel": 2
        },
        {
            "id": 7,
            "nombre": "1",
            "nivel": 3
        },
        {
            "id": 8,
            "nombre": "2",
            "nivel": 3
        },
        {
            "id": 9,
            "nombre": "3",
            "nivel": 3
        },
        {
            "id": 10,
            "nombre": "4",
            "nivel": 3
        },
        {
            "id": 11,
            "nombre": "5",
            "nivel": 3
        },
        {
            "id": 12,
            "nombre": "1",
            "nivel": 1
        },
        {
            "id": 13,
            "nombre": "2",
            "nivel": 1
        },
        {
            "id": 14,
            "nombre": "3",
            "nivel": 1
        },
        {
            "id": 15,
            "nombre": "4",
            "nivel": 1
        }
    ]

    //constructor(private setupinitialService: SetupinitialService){
    constructor(private router: Router){
        //localStorage.removeItem('ls_datasteps');
        
        /*setupinitialService.getCursos().subscribe((data: {}) => {
            this.ls_datasteps.cursos = data;
        });*/
        
    }
    /*
    ls_niveles: any=[];
    constructor(private initialService: InitialService){
        if( localStorage.niveles_v1 === undefined ){
            initialService.getNiveles().subscribe((data: {}) => {
                this.ls_niveles = data;
            });
        }else{
            this.ls_niveles=JSON.parse(localStorage.niveles_v1)
        }        
    } */   
}
@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const xrequest = req.clone({setHeaders: {'Content-Type':  'application/json','Authorization':'Token '+localStorage.userToken}});
        return next.handle(xrequest);
    }
}